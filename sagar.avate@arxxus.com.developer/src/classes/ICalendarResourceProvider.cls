/**
 * Calendar resource builder interface
 * Implement for appropriate classes to return the correct response
 * @author Arxxus Technology Partners
 */
public interface ICalendarResourceProvider {
	
	/**
	*get the calendar resource for showing on calendar
	*@param List of sObject,sObject field to calender resource properties maping
	*@return Calendar resource wrapper class list.
	*/
	List<CalendarResource> getCalendarResource(List<sObject> records,Map<String,String> propertyVsField);
}
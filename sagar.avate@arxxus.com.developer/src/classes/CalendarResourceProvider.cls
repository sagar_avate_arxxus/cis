/**
 * Calendar resource builder class
 * @author Arxxus Technology Partners
 */
public with sharing class CalendarResourceProvider implements ICalendarResourceProvider{
	
	//Calendar resource Required properties 
	private List<String> REQUIRED_PROPERTIES = new List<String>{
		'title'
	};

	/**
	*get the calendar resources for showing on calendar
	*@param List of sObject,sObject field to calender resource properties maping
	*@return Calendar resource wrapper class list.
	*/
	public List<CalendarResource> getCalendarResource(List<sObject> records,Map<String,String> propertyVsField){

		//validate all required field provided in map
		for(String property : REQUIRED_PROPERTIES){
			if(!propertyVsField.containsKey(property)){
				throw new CalenderResourceException('Invaild Parameters : Required property missing ('+property+')');
			}
		}

		//Create calendat resource for provided list of sObject
		List<CalendarResource> calendarResources = new List<CalendarResource>();
		for(sObject record : records){

			//create new calendar eent 
			calendarResources.add(new CalendarResource( 
				record.get(propertyVsField.get('title')) != null ? (String)record.get(propertyVsField.get('title')) : 'title',
				propertyVsField.containsKey('resourceId') ? (String)record.get(propertyVsField.get('resourceId')) : null,
				propertyVsField.containsKey('groupdId') ? (String)record.get(propertyVsField.get('groupdId')) : null,
				propertyVsField.containsKey('eventColor') ? (String)record.get(propertyVsField.get('eventColor')) : null
			));
		
		}
		return calendarResources; 

	}

	public class CalenderResourceException extends Exception{}
}
global class ProcessInboundEmail implements Messaging.InboundEmailHandler {

  global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,
    Messaging.InboundEnvelope envelope) {

    Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();

    Contact con;
    User use;
    try{
         con = [Select Id from contact where email = :envelope.fromAddress Limit 1];
    }catch(Exception ee){
        System.debug('#Exception in ProcessInboundEmail#'+ee.getMessage());
        result.message = 'Contact not present';
        result.success = false;
        return result;
    }

    try{
         use = [Select id from user where email = :email.ccAddresses Limit 1];
    }catch(Exception ee){
        System.debug('#Exception in ProcessInboundEmail#'+ee.getMessage());
        result.message = 'Please add ccAddresses';
        result.success = false;
        return result;
    }

    Task newTask = new Task();
    newTask.OwnerId = use.Id;
    newTask.Description = 'InboundEmailresult';
    newTask.Subject = 'Other';
    newTask.Status = 'Inbound Email'; 
    newTask.WhoId = con.id;

    insert newTask;
    System.debug('##newTask Inserted#'+newTask);

    result.message = 'Task Created.....';
    result.success = true;
    return result; 
    
    /*
    Contact contact = new Contact();
    contact.FirstName = email.fromname.substring(0,email.fromname.indexOf(' '));
    contact.LastName = email.fromname.substring(email.fromname.indexOf(' '));
    contact.Email = envelope.fromAddress;
    insert contact;
    System.debug('##Contact Inserted#'+contact);

    System.debug('##toAddresses#'+email.toAddresses);

    System.debug('====> Created contact '+contact.Id);

    if (email.binaryAttachments != null && email.binaryAttachments.size() > 0) {
      for (integer i = 0 ; i < email.binaryAttachments.size() ; i++) {
        Attachment attachment = new Attachment();
        // attach to the newly created contact record
        attachment.ParentId = contact.Id;
        attachment.Name = email.binaryAttachments[i].filename;
        attachment.Body = email.binaryAttachments[i].body;
        insert attachment;
      }
    }

    System.debug('Done!!!!!!!!!!!!!!!!!!!!!!!');

    return result;
    */
  }

}
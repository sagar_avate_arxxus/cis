/** 
 * Calendar event wrapper class
 * @author Arxxus Technology Partners
 */
public with sharing class CalendarEvent {
	
	/**
	*Calender event required paramerer Constructor 
	*/
	public CalendarEvent(String title,String startDate){
		this.title = title;
		this.start_Date = startDate;
	}

	/**
	*Calender event required and optional paramerer Constructor 
	*/
	public CalendarEvent(String title,String startDate,String endDate,String url,List<String> className,
		String resourceId,String color,String backgroundColor,String borderColor,String textColor,
		Boolean editable,Boolean allDay,String eventId){
		this.title = title;
		this.start_Date = startDate;
		this.end_Date = endDate;
		this.url = url;
		this.className = className;
		this.resourceId = resourceId;
		this.color = color;
		this.backgroundColor = backgroundColor;
		this.borderColor = borderColor;
		this.textColor = textColor;
		this.editable = editable;
		this.allDay = allDay;
		this.event_Id = eventId;
	}

	/**
	*Replace calendar event properties lables
	*/
	public static String originalLabels(String cEventsString){
		//replace eventid with Id, startDate with start and endDate with end 
		cEventsString = cEventsString.replaceAll('event_Id', 'Id');	
		cEventsString = cEventsString.replaceAll('start_Date', 'start');
		cEventsString = cEventsString.replaceAll('end_Date', 'end');

		return cEventsString;
	}


	@AuraEnabled
	public String event_Id{get;set;}
	@AuraEnabled
	public String title{get;set;}
	@AuraEnabled
	public Boolean allDay{get;set;}
	@AuraEnabled
	public String start_Date{get;set;}
	@AuraEnabled
	public String end_Date{get;set;}
	@AuraEnabled
	public String url{get;set;}
	@AuraEnabled
	public List<String> className{get;set;}
	@AuraEnabled
	public Boolean editable{get;set;}
	@AuraEnabled
	public String resourceId{get;set;}
	@AuraEnabled
	public String color{get;set;}
	@AuraEnabled
	public String backgroundColor{get;set;}
	@AuraEnabled
	public String borderColor{get;set;}
	@AuraEnabled
	public String textColor{get;set;}
}
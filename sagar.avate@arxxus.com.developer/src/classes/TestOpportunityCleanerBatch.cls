global with sharing class TestOpportunityCleanerBatch implements Database.Batchable<sObject>{
	

	global final String query;

	global TestOpportunityCleanerBatch(){
		query = 'SELECT Id FROM Opportunity WHERE Name LIKE \'%test%\'';
	}

	global Database.QueryLocator start(Database.BatchableContext BC){
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<Opportunity> opps){
		delete opps;
    }

	global void finish(Database.BatchableContext BC){
		//you need to send email notification regarding batch job status
		String subject = 'Test Opp deleted';
		String emailBody = 'Test Opp deletion comlete.......';
		String[] toAddress = new List<String>{'sagar.avate@arxxus.com'};
		EmailSender.sendEmail(subject,toAddress,emailBody);

	}

}
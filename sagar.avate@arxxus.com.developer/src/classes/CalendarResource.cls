/** 
 * Calendar resource wrapper class
 * @author Arxxus Technology Partners
 */
public with sharing class CalendarResource {
	
	/**
	*Calender resource required paramerer Constructor 
	*/
	public CalendarResource(String title,String resourceId,
		String groupdId,String eventColor){

		this.title = title;
		this.resource_Id = resourceId;
		this.groupdId = groupdId;
		this.eventColor = eventColor;
	}


	/**
	*Replace calendar resource properties lables
	*/
	public static String originalLabels(String cResourceString){
		//replace resource_Id with Id
		cResourceString = cResourceString.replaceAll('resource_Id', 'Id');	
		return cResourceString;
	}

	@AuraEnabled
	public String resource_Id{get;set;}
	@AuraEnabled
	public String title{get;set;}
	@AuraEnabled
	public String groupdId{get;set;}
	@AuraEnabled
	public String eventColor{get;set;}
}
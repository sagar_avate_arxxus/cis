/**
* Author : Arxxus
* Purpose : Thia class generate log records under account object
*  and populate all required values on it.
*/
public with sharing class COURIER_LogGenerator {
	
	
	public COURIER_LogGenerator(){

	}


	/**
	* This function generate log records.
	*/
	@TestVisible 
	public void generate(List<Account> accountList,
		String logType,
		COURIER_EmailAddressProvider.ICOURIER_EmailAddressProvider emailAddressProvider){

		List<Report_Courier_Log__c> logList = new List<Report_Courier_Log__c>();

		//traverse through account list
		for(Account acc: accountList){

			Report_Courier_Log__c log = new Report_Courier_Log__c();
			log.Account__c = acc.Id;
			log.Date__c = Date.today();
			log.Type__c = logType;
			//get to email address
			String toEmailAddress = emailAddressProvider.getToAddress(acc.Id);
			if(toEmailAddress != null){
				log.To_Email_Address__c = toEmailAddress;
				log.Error_Message__c = '';
			}else{
				log.Error_Message__c = ConstantHolder.PRIMARY_CONTACT_MISSING_ERROR;
			}
			log.CC_Email_Address__c = emailAddressProvider.getCcAddress(acc.Id);
			log.Unique_Id__c = acc.Id+'_'+logType+'_'+Date.today();

			logList.add(log);

		}

		upsert logList;

	}

}
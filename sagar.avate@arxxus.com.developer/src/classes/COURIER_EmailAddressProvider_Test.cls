@isTest
private class COURIER_EmailAddressProvider_Test {
	
	@isTest static void EmailAddressProvider_Test() {
		
		String logType = 'Daily';

		List<Contact> primaryContactList = new List<Contact>{
			new Contact(FirstName = 'ABC', LastName = 'XYZ', Email = 'primaryContact@Test.com',Daily__c = true),
			new Contact(FirstName = 'ABC', LastName = 'XYZ', Email = 'primaryContact@Test.com',Daily__c = true),
			new Contact(FirstName = 'ABC', LastName = 'XYZ', Email = 'primaryContact@Test.com',Daily__c = true),
			new Contact(FirstName = 'ABC', LastName = 'XYZ', Email = 'primaryContact@Test.com',Daily__c = true),
			new Contact(FirstName = 'ABC', LastName = 'XYZ',Daily__c = true)
		};
		insert primaryContactList;
		
		List<Account> accountList = new List<Account>{
			new Account(Name = 'Test1', Primary_Contact__c = primaryContactList[0].Id),
			new Account(Name = 'Test2', Primary_Contact__c = primaryContactList[1].Id),
			new Account(Name = 'Test3', Primary_Contact__c = primaryContactList[2].Id),
			new Account(Name = 'Test4', Primary_Contact__c = primaryContactList[3].Id),
			new Account(Name = 'Test5', Primary_Contact__c = primaryContactList[4].Id)
		};
		insert accountList;
		System.debug('#EMAIL:'+accountList[0].Primary_Contact__r.Email);

		List<Contact> secondaryContactList = new List<Contact>{
			new Contact(FirstName = 'ABC', LastName = 'XYZ', 
				Email = 'secondaryContact@Test.com',AccountId = accountList[0].Id,Daily__c = true),
			new Contact(FirstName = 'ABC', LastName = 'XYZ', 
				Email = 'secondaryContact@Test.com',AccountId = accountList[0].Id,Daily__c = true),
			new Contact(FirstName = 'ABC', LastName = 'XYZ', 
				Email = 'secondaryContact@Test.com',AccountId = accountList[1].Id,Daily__c = true),
			new Contact(FirstName = 'ABC', LastName = 'XYZ', 
				Email = 'secondaryContact@Test.com',AccountId = accountList[2].Id,Daily__c = true),
			new Contact(FirstName = 'ABC', LastName = 'XYZ', 
				AccountId = accountList[3].Id,Daily__c = true)
		};
		insert secondaryContactList;

		accountList = [SELECT Id,Primary_Contact__r.Email,Primary_Contact__r.Daily__c From Account];
		
		Test.startTest();
		COURIER_EmailAddressProvider emailAddProvider = new COURIER_EmailAddressProvider(accountList,logType);
		System.assertEquals('primarycontact@test.com',emailAddProvider.getToAddress(accountList[0].Id));
		System.assertEquals(null,emailAddProvider.getToAddress(accountList[4].Id));
		System.assertEquals('secondarycontact@test.com,secondarycontact@test.com',
			emailAddProvider.getCcAddress(accountList[0].Id));
		System.assertEquals(null,
			emailAddProvider.getCcAddress(accountList[4].Id));
		Test.stopTest();

	}
	
}
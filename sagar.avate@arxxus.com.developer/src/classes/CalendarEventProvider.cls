/**
 * Calendar event builder class
 * @author Arxxus Technology Partners
 */
public with sharing class CalendarEventProvider implements ICalendarEventProvider{
	
	//Calendar event Required properties 
	private List<String> REQUIRED_PROPERTIES = new List<String>{
		'title','startDate'
	};
	//Calendar event Optional properties 
	private List<String> ALL_PROPERTIES = new List<String>{
		'title','startDate','endDate','url','className','resourceId','color',
		'backgroundColor','borderColor','textColor','editable','allDay'
	};  

	/**
	*get the calendar events for showing on calendar
	*@param List of sObject,sObject field to calender event properties maping
	*@return Calendar event wrapper class list.
	*/
	public List<CalendarEvent> getCalendarEvent(List<sObject> records,Map<String,String> propertyVsField){

		//validate all required field provided in map
		for(String property : REQUIRED_PROPERTIES){
			if(!propertyVsField.containsKey(property)){
				throw new CalenderEventException('Invaild Parameters : Required property missing ('+property+')');
			}
		}

		//Create calendat events for provided list of sObject
		List<CalendarEvent> calendarEvents = new List<CalendarEvent>();
		for(sObject record : records){

			//Generate list of class name
			List<String> className;
			if(propertyVsField.containsKey('className')){
				String rClassName = (String)record.get(propertyVsField.get('className'));
				if(rClassName != null){
					className = rClassName.split(',');
				}
			}
			//create new calendar eent 
			calendarEvents.add(new CalendarEvent( 
				record.get(propertyVsField.get('title')) != null ? (String)record.get(propertyVsField.get('title')) : 'title',
				record.get(propertyVsField.get('startDate')) != null ?  String.valueOfGmt((Datetime)record.get(propertyVsField.get('startDate'))) : ''+Date.today(),
				propertyVsField.containsKey('endDate') ? String.valueOfGmt((Datetime)record.get(propertyVsField.get('endDate'))) : null,
				propertyVsField.containsKey('url') ? (String)record.get(propertyVsField.get('url')) : null,
				className,
				propertyVsField.containsKey('resourceId') ? (String)record.get(propertyVsField.get('resourceId')) : null,
				propertyVsField.containsKey('color') ? (String)record.get(propertyVsField.get('color')) : null,
				propertyVsField.containsKey('backgroundColor') ? (String)record.get(propertyVsField.get('backgroundColor')) : null,
				propertyVsField.containsKey('borderColor') ? (String)record.get(propertyVsField.get('borderColor')) : null,
				propertyVsField.containsKey('textColor') ? (String)record.get(propertyVsField.get('textColor')) : null,
				propertyVsField.containsKey('editable') ? (Boolean)record.get(propertyVsField.get('editable')) : true,
				propertyVsField.containsKey('allDay') ? (Boolean)record.get(propertyVsField.get('editable')) : false,
				propertyVsField.containsKey('eventId') ? (String)record.get(propertyVsField.get('eventId')) : null			
			));
		
		}
		return calendarEvents; 

	}

	public class CalenderEventException extends Exception{}

}
public with sharing class EmailSender {

	//Send email without Attachment
	public static void sendEmail(String subject, String [] toAddress, String emailBody) {

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();     

        mail.setToAddresses(toAddress);
        
        //subject
        mail.setSubject(subject);     
        mail.setPlainTextBody(emailBody);
        mail.setUseSignature(false);       
        //sending the mail 
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}
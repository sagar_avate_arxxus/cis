/**
 * Calendar event builder interface
 * Implement for appropriate classes to return the correct response
 * @author Arxxus Technology Partners
 */
public interface ICalendarEventProvider {

	/**
	*get the calendar events for showing on calendar
	*@param List of sObject,sObject field to calender event properties maping
	*@return Calendar event wrapper class list.
	*/
	List<CalendarEvent> getCalendarEvent(List<sObject> records,Map<String,String> propertyVsField);

}
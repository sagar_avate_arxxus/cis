/**
 * Scheduler controller class
 * @author Arxxus Technology Partners
 */
public with sharing class CONT_SchedulerController {
	
	public CONT_SchedulerController() {
		
	}

	@AuraEnabled
	public static String getCalenderEvents(){
		
		List<Event__c> events = [SELECT Id,Name,Start_Date__c,End_Date__c FROM Event__c];
		System.debug('events==========>'+events);

		Map<String,String> propertyVsField = new Map<String,String>{
			'title' => 'Name',
			'startDate' => 'Start_Date__c',
			'endDate' => 'End_Date__c',
			'eventId' => 'Id'
		};

		ICalendarEventProvider cEventProvider = new CalendarEventProvider();
		List<CalendarEvent> cEvents = cEventProvider.getCalendarEvent(events,propertyVsField);
		System.debug('cEvents==========>'+cEvents);
		String jsonString = CalendarEvent.originalLabels(System.JSON.serialize(cEvents));
		System.debug('jsonString==========>'+jsonString);
		return jsonString;
	}
}
public with sharing class MyController {

	public Opportunity mysObject {get;set;}
    public String name {get;set;}

    
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public MyController() {
         String opportunityId = ApexPages.CurrentPage().getParameters().get('id');
         mysObject = [SELECT Id,name from Opportunity WHERE Id = :opportunityId];
    }


    
}
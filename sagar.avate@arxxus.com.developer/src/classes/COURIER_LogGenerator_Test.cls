@isTest
private class COURIER_LogGenerator_Test {
	
	@isTest static void LogGenerator_Test(){

		String logType = 'Daily';
		
		List<Account> accountList = new List<Account>{
			new Account(Name = 'Test1'),
			new Account(Name = 'Test2'),
			new Account(Name = 'Test3'),
			new Account(Name = 'Test4'),
			new Account(Name = 'Test5')
		};
		insert accountList;

		Test.startTest();
		COURIER_LogGenerator logGenerator = new COURIER_LogGenerator();
		logGenerator.generate(accountList,logType,new MockEmaillAddressProvider());
		Test.stopTest();

		List<Report_Courier_Log__c> logList = [SELECT Id FROM Report_Courier_Log__c];
		System.assertEquals(5,logList.size());
	}

	public class MockEmaillAddressProvider implements COURIER_EmailAddressProvider.ICOURIER_EmailAddressProvider{

			public MockEmaillAddressProvider(){}

			public String getToAddress(Id accountId){

				if(Math.mod((Integer)Math.round(Math.random() * (10 - 2)) + 2,2) == 0){
					return 'toAddress@test.Email';
				}else{
					return null;
				}				
			}
			
			public String getCcAddress(Id accountId){
				return 'ccAddress@test.Email';	
			}
	}
	
}
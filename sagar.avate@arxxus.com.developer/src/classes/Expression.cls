/**
*To implement the Composite Design Pattern
*/
public interface Expression {
	
	Expression add(Expression exp);
	Expression set(String name,Boolean value);
	Boolean evaluate();
}
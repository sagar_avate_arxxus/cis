/**
* Author : Arxxus
* Purpose : This class holds all constants.
*/
public with sharing class ConstantHolder {
	
	//COURIER log constants
	public static final Map<String,String> LOG_TYPE_VS_APINAME_MAP = new Map<String,String>{
		'Daily' => 'Daily__c',
		'Weekly' => ''
	};

	public static final String PRIMARY_CONTACT_MISSING_ERROR = 'Primary contact missing on account';

}
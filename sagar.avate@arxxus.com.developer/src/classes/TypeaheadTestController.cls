public with sharing class TypeaheadTestController {
	
	public String whereClause {get;set;}

	public TypeaheadTestController() {
		whereClause = null;
	}
}
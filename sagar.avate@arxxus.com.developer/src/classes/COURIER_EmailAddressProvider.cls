/**
* Author : Arxxus
* Purpose : This class provide to email address and cc email address while log 
* record generation.
*/
public with sharing class COURIER_EmailAddressProvider implements COURIER_EmailAddressProvider.ICOURIER_EmailAddressProvider{
	

	//Contact Id Vs List of related contact emails.
    @TestVisible 
    private Map<Id,String> accIdVsCcEmailsMap;
    //Account Id Vs Primary Contact Id Map.
    @TestVisible 
    private Map<Id,String> accIdVsToEmailMap;
    //Contact id set
    private Set<Id> contactIdSet;

	public COURIER_EmailAddressProvider(List<Account> accountList,String logType){
		fetchToAddress(accountList,logType);
		fetchCCAddress(logType);
	}

	/**
	* This function fetch Account Vs Primary Contact Id Map
	*/
	@TestVisible
	private void fetchToAddress(List<Account> accountList,String logType){
		//init
		accIdVsToEmailMap = new Map<Id,String>();
		contactIdSet = new Set<Id>();
		for(Account acc : accountList){

			//check if account primary contact not null and check log type.
			if(acc.Primary_Contact__r.Email != null 
				&& acc.getSobject('Primary_Contact__r').get(ConstantHolder.LOG_TYPE_VS_APINAME_MAP.get(logType)) == true){
				//put account id Vs primary contact email. 
				accIdVsToEmailMap.put(acc.Id,acc.Primary_Contact__r.Email);
				//add primary contact in to set.
				contactIdSet.add(acc.Primary_Contact__c);
			}
		}
	}

	/**
    * Fetch contact Id Vs List of related contact emails.
	*/
	@TestVisible
	private void fetchCCAddress(String logType){
		//init
		accIdVsCcEmailsMap = new Map<Id,String>();
		//query field 
		String queryFields = 'Id,Email,AccountId';
		//account id set
        Set<Id> accIdSet = accIdVsToEmailMap.keySet();
        //fetch contact
        for(Contact con : Database.query('SELECT '+queryFields+' FROM Contact WHERE AccountId IN :accIdSet'
                    +' AND Id NOT IN :contactIdSet AND '+ConstantHolder.LOG_TYPE_VS_APINAME_MAP.get(logType)+' = true')){
            
            //check if email null
            if(con.Email == null){
                continue;
            }
            if(accIdVsCcEmailsMap.containsKey(con.AccountId)){
                accIdVsCcEmailsMap.put(con.AccountId,accIdVsCcEmailsMap.get(con.AccountId)+','+con.Email);
            }else{
                accIdVsCcEmailsMap.put(con.AccountId,con.Email);
            }
        }
	}

	/**
	* This function return to address for account
	*/
	public String getToAddress(Id accountId){
		if(accIdVsToEmailMap.containsKey(accountId)){
			return accIdVsToEmailMap.get(accountId);
		}else{
			return null;
		}
	}

	/**
	* This function return to address for account
	*/
	public String getCcAddress(Id accountId){
		if(accIdVsCcEmailsMap.containsKey(accountId)){
			return accIdVsCcEmailsMap.get(accountId);
		}else{
			return null;
		}
	}

	public Interface ICOURIER_EmailAddressProvider{
		String getToAddress(Id accountId);
		String getCcAddress(Id accountId);
	}
}